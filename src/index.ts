/*
* Task 1: Once decorator
*/
function once(_target: any, _propertyKey: string, descriptor: PropertyDescriptor) {
    const original = descriptor.value;
    var isCalled = false
    var result: ReturnType<typeof original>;
    
    descriptor.value = function (...args) {
        if (!isCalled) {
            result = original.call(this, ...args);
            isCalled = true;
        }
        return result;
    }
}

/*
* Task 2: Identifier decorator
*/
function identifier(label: string) {
    return function(target: Function) {
        target.prototype.identify = function() {
            return `${target.name}-${label}`;
        };
    }
}

export {once, identifier}
